package at.itkollegimst.gasser.pos1makro.test2.buchhandlung.exceptions;

public class BestellungNotFoundException extends Exception{
	public BestellungNotFoundException() {
		super("Bestellung nicht gefunden");
	}
}
