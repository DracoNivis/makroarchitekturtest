package at.itkollegimst.gasser.pos1makro.test2.buchhandlung.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;

@Entity
@Table(name = "bestellungen")
@Data
public class Bestellung extends AbstractAggregateRoot<Bestellung> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(unique = true)
	private String bestellNummer;
	private String titel;
	private BestellStatus status;
	
	public Bestellung() {
	}
	
	public Bestellung(Bestellung bestellung) {
		this.bestellNummer = bestellung.getBestellNummer();
		this.titel = bestellung.getTitel();
		this.status = BestellStatus.BESTELLT;
	}
	
}
