package at.itkollegimst.gasser.pos1makro.test2.buchhandlung.infrastructure.service;

import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.shareddomain.events.BuchBestelltEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RabbitMQSender {
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	public RabbitMQSender(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}
	
	@Value("${spring.rabbitmq.exchange}")
	private String exchange;
	
	@Value("${spring.rabbitmq.routingkey}")
	private String routingkey;
	
	public void send(BuchBestelltEvent buchBestelltEvent) {
		rabbitTemplate.convertAndSend(exchange, routingkey, buchBestelltEvent);
		log.info("Bestellung " + buchBestelltEvent.getBestellTitel() + " an Druckerei gesendet");
	}
}
