package at.itkollegimst.gasser.pos1makro.test2.buchhandlung.infrastructure.service;

import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.api.rest.BestellungController;
import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.domain.BestellStatus;
import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.shareddomain.events.BuchGedrucktEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class RabbitMQConsumer {
	private BestellungController bestellungController;
	
	//@RabbitListener(queues = "bestellungPrinted.queue")
	public void bestellungPrintedEventMessageReceived(BuchGedrucktEvent buchGedrucktEvent) {
		log.info("Buch gedruckt, Jetzt abholbereit");
	bestellungController.updateBestellStatus(buchGedrucktEvent.getBestellNummer(), BestellStatus.ABHOLBEREIT);
	}
}
