package at.itkollegimst.gasser.pos1makro.test2.buchhandlung.infrastructure.repository;

import at.itkollegimst.gasser.pos1makro.test2.buchhandlung.domain.Bestellung;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BestellungRepository extends JpaRepository<Bestellung, Long> {
	Optional<Bestellung> findByBestellNummer(String bestellNummer);
	void deleteByBestellNummer(String bestellNummer);
	Optional<Bestellung> getByBestellNummer(String bestellNummer);
}
