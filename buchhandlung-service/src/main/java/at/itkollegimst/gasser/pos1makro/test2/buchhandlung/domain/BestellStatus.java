package at.itkollegimst.gasser.pos1makro.test2.buchhandlung.domain;

public enum BestellStatus {
	BESTELLT, ABHOLBEREIT
}
